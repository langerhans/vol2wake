package de.langerhans.vol2wake;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.execution.CommandCapture;

public class Settings extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Set background of the window
		getWindow().setBackgroundDrawableResource(R.drawable.nexus);
		//We are requesting this to make the AB translucent later
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		
		setContentView(R.layout.activity_main);
		
		//Fancy fonts...
		Typeface tf = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
		TextView t1 = (TextView) findViewById(R.id.textView1);
		TextView t2 = (TextView) findViewById(R.id.textView2);
		Button b1 = (Button) findViewById(R.id.button1);
		Button b2 = (Button) findViewById(R.id.button2);
		t1.setTypeface(tf);
		t2.setTypeface(tf);
		b1.setTypeface(tf);
		b2.setTypeface(tf);
		
		b1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				initScriptHandler(true);
				Toast.makeText(getApplicationContext(), getString(R.string.toast_enable), Toast.LENGTH_LONG).show();
			}
		});
		b2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				initScriptHandler(false);
				Toast.makeText(getApplicationContext(), getString(R.string.toast_enable), Toast.LENGTH_LONG).show();
			}
		});
		
		//Translucent AB with Roboto font.
		getActionBar().setBackgroundDrawable(new ColorDrawable(Color.argb(128, 0, 0, 0)));
		SpannableString s = new SpannableString(getString(R.string.app_name));
		s.setSpan(new TypefaceSpan(this), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getActionBar().setTitle(s);
	}
	
	public static void initScriptHandler(Boolean newState)
	{
		if(newState)
		{
			CommandCapture command = new CommandCapture(0,
					"mount -o rw,remount /system",
					"echo \"#!/system/bin/sh\n\necho 1 > /sys/keyboard/vol_wakeup\nchmod 444 /sys/keyboard/vol_wakeup\" > /etc/init.d/89s5tvol2wake",
					"chmod 755 /system/etc/init.d/89s5tvol2wake",
					"sed -i 's/\\(key [0-9]\\+\\s\\+VOLUME_\\(DOWN\\|UP\\)$\\)/\\1   WAKE_DROPPED/gw /system/usr/keylayout/Generic.kl' /system/usr/keylayout/Generic.kl",
					"mount -o ro,remount /system");
			try {
				RootTools.getShell(true).add(command).waitForFinish();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else
		{
			CommandCapture command = new CommandCapture(0,
					"mount -o rw,remount /system",
					"rm -f /etc/init.d/89s5tvol2wake",
					"sed -i 's/\\(key [0-9]\\+\\s\\+VOLUME_\\(DOWN\\|UP\\)\\)\\s\\+WAKE_DROPPED/\\1/gw /system/usr/keylayout/Generic.kl' /system/usr/keylayout/Generic.kl",
					"mount -o ro,remount /system");
		    try {
				RootTools.getShell(true).add(command).waitForFinish();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Span with a typeface (Removed Cache cause we use it just once.
	 * @author http://stackoverflow.com/a/15181195/1902129
	 *
	 */
	public class TypefaceSpan extends MetricAffectingSpan {

	    private Typeface mTypeface;

	    public TypefaceSpan(Context context) {
	        if (mTypeface == null) {
	            mTypeface = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
	        }
	    }

	    @Override
	    public void updateMeasureState(TextPaint p) {
	        p.setTypeface(mTypeface);
	        p.setFlags(p.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
	    }

	    @Override
	    public void updateDrawState(TextPaint tp) {
	        tp.setTypeface(mTypeface);
	        tp.setFlags(tp.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
	    }
	}
}
